const int TRIG_PIN = 12;
const int ECHO_PIN = 13;
const int led = 11;

void setup() {
  Serial.begin(9600);
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(led, OUTPUT);
  Serial.println("Start");
}

int counts = 0;
void loop()
{
  long duration, distanceCm, distanceIn;
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  duration = pulseIn(ECHO_PIN, HIGH);

  // convert the time into a distance
  distanceCm = duration / 29.1 / 2 ;
  distanceIn = duration / 74 / 2;

  if (distanceCm <= 0) {
    Serial.println("Out of range");
  }
  else {
    Serial.print(counts++);
    Serial.print(" : ");
    Serial.print(distanceIn);
    Serial.print("in, ");
    Serial.print(distanceCm);
    Serial.print("cm");
    Serial.println();

    if (distanceCm < 10) {
      digitalWrite(led, HIGH);
      Serial.println("HIGH");
    }
    else if (distanceCm >= 10) {
      digitalWrite(led, LOW);
    }
  }
  delay(1000);
}
